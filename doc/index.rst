Location Measurements
####################

The Location Measurements module adds this following measurements to Locations:

- Length
- Height
- Width
- Weight
