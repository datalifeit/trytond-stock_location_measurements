# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:stock.location,height:"
msgid "Height"
msgstr "Višina"

msgctxt "field:stock.location,height_digits:"
msgid "Height Digits"
msgstr "Decimalke za višino"

msgctxt "field:stock.location,height_uom:"
msgid "Height Uom"
msgstr "ME za višino"

msgctxt "field:stock.location,length:"
msgid "Length"
msgstr "Dolžina"

msgctxt "field:stock.location,length_digits:"
msgid "Length Digits"
msgstr "Decimalke za dolžino"

msgctxt "field:stock.location,length_uom:"
msgid "Length Uom"
msgstr "ME za dolžino"

msgctxt "field:stock.location,width:"
msgid "Width"
msgstr "Širina"

msgctxt "field:stock.location,width_digits:"
msgid "Width Digits"
msgstr "Decimalke za širino"

msgctxt "field:stock.location,width_uom:"
msgid "Width Uom"
msgstr "ME za širino"

msgctxt "view:stock.location:"
msgid "Measurements"
msgstr "Mere"
