# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class LocationMeasuresTestCase(ModuleTestCase):
    """Location measurements Test module"""
    module = 'stock_location_measurements'

    def setUp(self):
        super(LocationMeasuresTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(LocationMeasuresTestCase))
    return suite
